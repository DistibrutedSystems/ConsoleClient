package galgeleg.clients;

import brugerautorisation.RMI.IBrugerAutorisationRMI;
import galgeleg.interfaces.RMI.IGalgelogikRMI;
import galgeleg.interfaces.RMI.IHighscore;

import javax.swing.*;
import java.rmi.Naming;


public class BenytGalgelogikModServerRMI {

    static IGalgelogikRMI spil = null;
    static IHighscore highscore = null;
    static int score = 0;
    static String username;
    static String password;

    public static void main(String[] args) throws Exception {

        //IGalgelogikRMI spil = (IGalgelogikRMI) Naming.lookup("RMI://localhost:2008/galgelogik");
        IBrugerAutorisationRMI ba = (IBrugerAutorisationRMI) Naming.lookup("rmi://dyrsted.duckdns.org:4562/galgelogik");
//    highscore = (IHighscore) Naming.lookup("RMI://localhost:2008/Highscore");


        while (spil == null) {
            username = JOptionPane.showInputDialog("Brugernavn");
            password = JOptionPane.showInputDialog("Adgangskode");
            if (username.equals("null") || password.equals("null")) {
                System.exit(0);
            }
            try {
                spil = ba.login(username, password);
            } catch (IllegalArgumentException e) {

            }
        }

        startGame();

        return;

    }

    private static void startGame() throws Exception {

        spil.nulstil();
        score = 0;
        try {
            spil.hentOrdFraDr();
        } catch (Exception e) {
            e.printStackTrace();
        }
        spil.logStatus();

        String letter;
        String status;
        while (!spil.erSpilletSlut()) {
            letter = JOptionPane.showInputDialog(spil.getStatusBesked());

            if (letter == null) {
                System.exit(0); // Don't crash when exiting pls
            }
            spil.gætBogstav(letter);
            score++;
        }

        if (spil.erSpilletVundet()) {
            JOptionPane.showMessageDialog(null, "You won! \nThe word was: " + spil.getOrdet() + "\nYour score: " + score);
            status = "You won!";
        } else {
            JOptionPane.showMessageDialog(null, "You lost... \nThe word was: " + spil.getOrdet() + "\nYour score: " + score);
            status = "You Lost!";
        }
        int response = JOptionPane.showConfirmDialog(null, "Do you want to continue?", status,
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (response == JOptionPane.NO_OPTION) {
          /*
          if(tryHighscore()){
              String scoreList = "";
              for(ArrayList<String> score : highscore.get()){
                  scoreList = scoreList + "\n"+score.get(0) + " - " + score.get(1);
              }
              JOptionPane.showMessageDialog(null, "NEW HIGHSCORE! " + scoreList);
          }*/
        } else if (response == JOptionPane.YES_OPTION) {
            startGame();
        } else if (response == JOptionPane.CLOSED_OPTION) {
            System.exit(0);
        }

    }
  /*
  public static boolean tryHighscore() throws Exception{
        return highscore.submitScore(""+score, username);
  }*/
}
